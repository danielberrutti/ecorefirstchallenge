package main

import "strconv"

func validateMatrix(records [][]string) (bool, string) {
	//check if all matrix members are numbers
	for i := 0; i < len(records); i++ {
		for j := 0; j < len(records[i]); j++ {
			if _, err := strconv.ParseInt(records[i][j], 10, 64); err != nil {
				return true, "All members of the matrix must be integer numbers"
			}
		}
	}

	return false, ""
}
