# League Backend Challenge

Small system in Go language providing the five requested operations endpoints.


There are 6 files included:


	1. main.go : the entry point of the system, which runs the backend server
	
	
	2. operations.go : contains the functions that actually perform the operations
	
	
	3. validations.go : functions that validate the input
	
	
	4. challenge1_test.go : unit tests of the operations
	
	
	5. matrix.csv : a sample input file
	
	
	6. this README file
	
	
	
How to run the program:

1. Make sure you have the Go language runtime properly installed and configured according to your OS, check https://golang.org/doc/install for details.


2. Clone this project or download the files above into a local folder (i.e. "/home/challenge1" (Unix-Linux) or "C:\challenge1" (Windows)


3. Open a system terminal or command line in the created folder


4. Modify the matrix.csv file as you like, provided that it contains a square matrix of integer numbers.

	
	To run the system:
	
	
		1 - in the command line, type 'go run .'
		
		
		2 - in a second command line, type "curl -F file=@[PATH_TO_FILE]\matrix.csv" "localhost:8080/[OPERATION]"' , 
		
		
		being [OPERATION] the operation you want to perform on the matrix, one of echo, invert, flatten, sum or multiply.
		
	To run the unit tests:
	
		In the command line, type 'go test'
