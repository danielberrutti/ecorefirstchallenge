package main

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"strings"
)

// Run with
//		go run .
// Send request with:
//		curl -F 'file=@/path/matrix.csv' "localhost:8080/echo"
//

func main() {
	//define function to handle requests
	handleRequest := func(w http.ResponseWriter, r *http.Request) {
		handleRequest(w, r)
	}
	//all requests to port 8080 will be handled by handleRequest function
	http.HandleFunc("/", handleRequest)
	http.ListenAndServe(":8080", nil)
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("file")
	if err != nil {
		w.Write([]byte(fmt.Sprintf("error %s", err.Error())))
		return
	}
	//reject files with extension other than .csv
	if !strings.HasSuffix(header.Filename, ".csv") {
		w.Write([]byte(fmt.Sprintf("error %s", "Only .csv files are allowed")))
		return
	}
	defer file.Close()
	//parse the file contents into the matrix 'records'
	records, err := csv.NewReader(file).ReadAll()
	if err != nil {
		//this error comes when the matrix is not square
		if strings.Contains(err.Error(), "wrong number of fields") {
			w.Write([]byte(fmt.Sprintf("error %s", "The matrix must be square")))
		} else {
			w.Write([]byte(fmt.Sprintf("error %s", err.Error())))
		}
		return
	}
	/* call the validateMatrix function, which will check if all
	matrix members are integers
	*/
	if hasError, message := validateMatrix(records); hasError {
		w.Write([]byte(fmt.Sprintf("error %s", message)))
		return
	}
	//get the operation requested from the url
	url := r.URL.EscapedPath()
	//decide which function to call based on the requested operation
	switch url {
	case "/echo":
		fmt.Fprint(w, handleEcho(records))
	case "/invert":
		fmt.Fprint(w, handleInvert(records))
	case "/flatten":
		fmt.Fprint(w, handleFlatten(records))
	case "/sum":
		sum, err := handleSum(records)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("error %s", err.Error())))
		}
		fmt.Fprint(w, sum)
	case "/multiply":
		prod, err := handleMultiply(records)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("error %s", err.Error())))
		}
		fmt.Fprint(w, prod)
	default:
		fmt.Fprint(w, "Operation "+url+" not implemented")
	}
}
