package main

import (
	"fmt"
	"strconv"
	"strings"
)

func handleEcho(records [][]string) string {
	var response string
	for _, row := range records {
		//prints the matrix as is
		response = fmt.Sprintf("%s%s\n", response, strings.Join(row, ","))
	}
	return response
}

func handleInvert(records [][]string) string {
	//create a matrix with the same dimension of the input one
	inverse := make([][]string, len(records))
	for i := 0; i < len(records); i++ {
		inverse[i] = make([]string, len(records))
		for j := 0; j < len(records); j++ {
			/*
				make the [j][i] member of the original matrix
				the [i][j] member of the inverted one
			*/
			inverse[i][j] = records[j][i]
		}
	}
	var response string
	//create the string representing the inverted matrix and return it
	for _, row := range inverse {
		response = fmt.Sprintf("%s%s\n", response, strings.Join(row, ","))
	}
	return response
}

func handleFlatten(records [][]string) string {
	var response string
	line := make([]string, len(records))
	lineIndex := 0
	//make each row of the matrix a comma-separated string
	for _, row := range records {
		line[lineIndex] = fmt.Sprintf("%s%s", line[lineIndex], strings.Join(row, ","))
		lineIndex++
	}
	//join all lines as comma-sepparated strings
	response = fmt.Sprintf("%s%s", response, strings.Join(line, ","))
	return response
}

func handleSum(records [][]string) (string, error) {
	//the sum starts with zero
	sum := 0
	for i := 0; i < len(records); i++ {
		for j := 0; j < len(records); j++ {
			/*
				try to convert each member of the string into a number
				(we already made sure all are integers, at validateMatrix function)
			*/
			num, err := strconv.Atoi(records[i][j])
			if err != nil {
				return "", err
			}
			//add the matrix member to the sum
			sum += num
		}
	}
	//return the sum as a string
	return strconv.Itoa(sum), nil
}

func handleMultiply(records [][]string) (string, error) {
	//the product starts with 1
	prod := 1
	for i := 0; i < len(records); i++ {
		for j := 0; j < len(records); j++ {
			/*
				try to convert each member of the string into a number
				(we already made sure all are integers, at validateMatrix function)
			*/
			num, err := strconv.Atoi(records[i][j])
			if err != nil {
				return "", err
			}
			//multiplies the member by the previous product
			prod = prod * num
		}
	}
	//returns the product as a string
	return strconv.Itoa(prod), nil
}
