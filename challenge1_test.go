package main

import (
	"strings"
	"testing"
)

var matrix = [][]string{
	{"1", "2", "3", "4"},
	{"5", "6", "7", "8"},
	{"9", "10", "11", "12"},
	{"13", "14", "15", "16"}}

func TestEcho(t *testing.T) {
	echo := handleEcho(matrix)
	normalizedEcho := strings.Replace(echo, "\n", ",", -1)
	if normalizedEcho != "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16," {
		t.Errorf("Echo was incorrect, got: %s, expected: %s.", echo, `1,2,3,4
		5,6,7,8
		9,10,11,12
		13,14,15,16`)
	}

}

func TestInvert(t *testing.T) {
	inverted := handleInvert(matrix)
	normalizedInvert := strings.Replace(inverted, "\n", ",", -1)
	if normalizedInvert != "1,5,9,13,2,6,10,14,3,7,11,15,4,8,12,16," {
		t.Errorf("Invert was incorrect, got: %s, expected: %s.", inverted, `1,5,9,13
		2,6,10,14
		3,7,11,15
		4,8,12,16`)
	}

}

func TestFlatten(t *testing.T) {
	flattened := handleFlatten(matrix)
	if flattened != "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16" {
		t.Errorf("Flatten was incorrect, got %s, expected %s.", flattened, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16")
	}
}

func TestSum(t *testing.T) {
	sum, err := handleSum(matrix)
	if err != nil {
		t.Errorf("Sum operation threw an error")
	}
	if sum != "136" {
		t.Errorf("Sum was incorrect, got %s, expected %s.", sum, "136")
	}
}

func TestMultiply(t *testing.T) {
	prod, err := handleMultiply(matrix)
	if err != nil {
		t.Errorf("Multiply operation threw an error")
	}
	if prod != "20922789888000" {
		t.Errorf("Multiply was incorrect, got %s, expected %s.", prod, "20922789888000")
	}
}
